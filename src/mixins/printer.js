import pdfMake from "pdfmake";
import pdfFonts from "../assets/custom-font";

export default {
	methods: {
		lastPrice(total, discount) {
			let price = total - discount;
			return price < 0 ? 0 : price;
		},
		initPDF(){
			pdfMake.vfs = pdfFonts.pdfMake.vfs; // 2. set vfs pdf font
			pdfMake.fonts = {
				// download default Roboto font from cdnjs.com
				Roboto: {
					normal: "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.66/fonts/Roboto/Roboto-Regular.ttf",
					bold: "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.66/fonts/Roboto/Roboto-Medium.ttf",
					italics: "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.66/fonts/Roboto/Roboto-Italic.ttf",
					bolditalics: "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.66/fonts/Roboto/Roboto-MediumItalic.ttf",
				},
				// Kanit Font
				Sarabun: {
					// 3. set Kanit font
					normal: "Sarabun-Regular.ttf",
					bold: "Sarabun-Medium.ttf",
					italics: "Sarabun-Italic.ttf",
					bolditalics: "Sarabun-MediumItalic.ttf",
				},
			};
		},
		download(owner, invoice) {
			this.initPDF()
			let signWording = owner == "นักเรียน" ? "ลงชื่อผู้ชำระเงิน" : "ลงชื่อผู้รับเงิน"
			let bodies = _.chain(invoice.items)
				.filter((item, idx) => {
					if (!["โรงเรียน", "ร้านค้า"].includes(owner)) {
						return true;
					}
					let seller = owner == "โรงเรียน" ? 1 : 2;
					// console.log("check seller", seller, item.product, owner);
					return seller == item.product.seller;
				})
				.map((item, idx) => {
					let content = {
						index: idx + 1,
						name: `${item.product.name} ${item.option.name}`,
						price: item.option.price,
						quantity: item.quantity,
						total: item.quantity * item.option.price,
						note: item.note,
					};
					return [
						{ text: content.index, alignment: "center" },
						{ text: content.name, alignment: "center" },
						{ text: content.price, alignment: "center" },
						{ text: content.quantity, alignment: "center" },
						{ text: content.total, alignment: "center" },
						{ text: content.note, alignment: "center" },
					];
				})
				.value();
			let totalPrice = _.chain(invoice.items).filter((item, idx) => {
				if (!["โรงเรียน", "ร้านค้า"].includes(owner)) {
					return true;
				}
				let seller = owner == "โรงเรียน" ? 1 : 2;
				// console.log("check seller", seller, item.product, owner);
				return seller == item.product.seller;
			}).reduce((sum,item,idx)=>{
				sum+= item.quantity * item.option.price
				return sum;
			},0).value()
			const docDefinition = {
				pageSize: "A4",
				pageOrientation: "landscape",
				content: [
					{ text: "โรงเรียนราชินีบูรณะ จังหวัดนครปฐม", fontSize: 18, alignment: "center", style: "header", bold: true, width: "*" },
					"",
					{ text: "ใบสั่งซื้อสำหรับ" + owner, style: "subheader", fontSize: 12, alignment: "center" },
					"",
					{
						style: "tableExample",
						color: "#444",
						alignment: "center",
						table: {
							widths: ["auto", "*", "auto", "auto", "auto", "auto"],
							headerRows: 5,
							body: [
								[{ text: "เลขที่ใบสั่งซื้อ", bold: true, style: "tableHeader", alignment: "center" }, { text: invoice.id, colSpan: 2, alignment: "center" }, {}, { text: "วันที่ออกใบ", bold: true, style: "tableHeader", alignment: "center" }, { text: dayjs(invoice.issued_at).locale("th").format("DD MMM BBBB HH:mm:ss"), colSpan: 2, alignment: "center" }, {}],
								[{ text: "ชื่อ", bold: true, style: "tableHeader", alignment: "center" }, { text: invoice.user.firstname, colSpan: 2, alignment: "center" }, {}, { text: "นามสกุล", bold: true, style: "tableHeader", alignment: "center" }, { text: invoice.user.lastname, colSpan: 2, alignment: "center" }, {}],
								[
									{ text: "ชั้น", style: "tableHeader", alignment: "center" },
									{ text: `ม. ${invoice.user.grade}/${invoice.user.class}`, alignment: "center" },
									{ text: "เลขที่", style: "tableHeader", alignment: "center" },
									{ text: invoice.user.class_number, alignment: "center" },
									{ text: "เบอร์โทรศัพท์", style: "tableHeader", alignment: "center" },
									{ text: invoice.user.phone_number, alignment: "center" },
								],
								[{ text: "ใบสั่งจองสินค้า ปีการศึกษา 2565", style: "tableHeader", alignment: "center", colSpan: 6 }, {}, {}, {}, {}, {}],
								[
									{ text: "ลำดับ", style: "tableHeader", alignment: "center" },
									{ text: "รายการ", style: "tableHeader", alignment: "center" },
									{ text: "ราคา (บาท)", style: "tableHeader", alignment: "center" },
									{ text: "จำนวน", style: "tableHeader", alignment: "center" },
									{ text: "รวมเป็นเงิน (บาท)", style: "tableHeader", alignment: "center" },
									{ text: "หมายเหตุ", style: "tableHeader", alignment: "center" },
								],
								...bodies,
								[
									{ text: "รวมเป็นเงิน", style: "tableHeader", alignment: "center" },
									{ text: totalPrice, alignment: "center" },
									{ text: "ส่วนลด", style: "tableHeader", alignment: "center" },
									{ text: invoice.user.discount?.value ?? 0, alignment: "center" },
									{ text: "ที่ต้องชำระ", style: "tableHeader", alignment: "center" },
									{ text: this.lastPrice(totalPrice, invoice.user.discount?.value ?? 0), alignment: "center" },
								],
								[{ text: signWording, style: "tableHeader", alignment: "center", colSpan: 5 }, {}, {}, {}, {}, { text: "", alignment: "center" }],
							],
						},
					},
					{ width: "*", text: "" },
				],
				styles: {
					header: {
						fontSize: 18,
						bold: true,
						margin: [0, 0, 0, 10],
					},
					subheader: {
						fontSize: 16,
						bold: true,
						margin: [0, 10, 0, 5],
					},
					tableExample: {
						margin: [0, 5, 0, 15],
					},
					tableHeader: {
						bold: true,
						fontSize: 13,
						color: "black",
					},
				},
				defaultStyle: {
					font: "Sarabun",
				},
			};
			pdfMake.createPdf(docDefinition).open();
		},
		async downloadMultiple(owner, invoiceList ,grade, classRoom) {

			let loop = invoiceList.reduce((result,item)=>{
				return result.concat(item.items)
			},[]).filter((item)=>item.product.seller===(owner==='โรงเรียน'?1:2))
			// console.log("loop",loop);
			var group = _.groupBy(loop, (item)=>{
				return [item.product.name,item.option.name,item.product.seller]
			})
			var group2 = _.groupBy(loop, (item)=>{
				return [item.product.name]
			})
			var list = Object.values(group).reduce((result,item,index)=>{
				var sum = _.sumBy(item,'quantity')
				var name = item[0].product.name
				var option = item[0].option.name
				var price = item[0].option.price

				var totalQuantityByName = _.sumBy(group2[item[0].product.name],'quantity')
				var priceTotalOption = group2[name].map((item)=>{
					return {priceTotalOption:item.option.price*item.quantity}
				})
				var totalPriceyByName = _.sumBy(priceTotalOption,'priceTotalOption')
				result.push({
					name:name,
					quantity:sum,
					option:option,
					price:price,
					total:sum*price,
					totalQuantityByName:totalQuantityByName,
					totalPriceyByName:totalPriceyByName
				})
				return result
			},[])
			var orderBy = _.orderBy(list, ['name'])
			var addTotal = orderBy.reduce((result,item,index)=>{
				result.push(item)
				if ( _.get(orderBy,`[${index}].name`,'') !== _.get(orderBy,`[${index+1}].name`)) {
					result.push({
						name:'รวม',
						quantity:'',
						option:'',
						price:'',
						total:'',
						totalQuantityByName:item.totalQuantityByName,
						totalPriceyByName:item.totalPriceyByName
					})
				}
				return result
			},[])
			var AllTotal = addTotal.filter((item)=>item.name==='รวม')
			var totalPriceAll = _.sumBy(AllTotal,'totalPriceyByName')
			var totalQuantityAll = _.sumBy(AllTotal,'totalQuantityByName')
			
			var bodyTable = addTotal.map((item)=>{
				return  item.name==='รวม'?[
					{ text: item.name, bold: true, style: "tableHeader", alignment:  "right" ,colSpan: 3},
					{},
					{},
					{ text: item.totalQuantityByName, bold: true, style: "tableHeader", alignment: "center" },
					{ text: item.totalPriceyByName, bold: true, style: "tableHeader", alignment: "center" },
				]:[
					{ text: item.name, bold: true, style: "tableHeader", alignment:  "left" },
					{ text: item.option, bold: false, alignment: "center" },
					{ text: item.price, bold: false, alignment: "center" },
					{ text: item.quantity, bold: false, alignment: "center" },
					{ text: item.total, bold: false, alignment: "center" },
				]
			})
			
			var app = this;
			this.initPDF();

			var allContent = []

			if(invoiceList.length >0){
				for(const invoice of invoiceList ){
					var itemIter = []
					if(owner == 'ร้านค้า'){
						itemIter = _.filter(invoice.items,(i)=>i.product.seller == 2)
					}else if (owner == 'โรงเรียน'){
						itemIter = _.filter(invoice.items, (i)=>i.product.seller == 1)
					}

					var bodies = await app.initBody(itemIter,owner);
					var totalPrice = await app.calTotalPrice(itemIter,owner);
				
					// console.log('isLastPage :: ',isLastPage , invoiceList.indexOf(invoice))
					var content = await app.initConTent(owner,bodies,invoice,totalPrice);
					// console.log("content",content);
					if(bodies.length >0){
						allContent = allContent.concat(content)
					}
					
					// console.log('bodies >> ' ,bodies)
					// console.log('totalPrice >> ' ,totalPrice)
					// console.log('content >> ' ,content)
				}

			}
			allContent = allContent.concat( await app.initConTentTableTotal(bodyTable,totalQuantityAll,totalPriceAll ,grade, classRoom,owner))
			delete allContent[allContent.length-2].pageBreak
			// console.log('allContent  >> ',allContent)
			const docDefinition = {
				pageSize: "A4",
				pageOrientation: "landscape",
				content: allContent,
				styles: {
					header: {
						fontSize: 18,
						bold: true,
						margin: [0, 0, 0, 10],
					},
					subheader: {
						fontSize: 16,
						bold: true,
						margin: [0, 10, 0, 5],
					},
					tableExample: {
						margin: [0, 5, 0, 15],
					},
					tableHeader: {
						bold: true,
						fontSize: 13,
						color: "black",
					},
				},
				defaultStyle: {
					font: "Sarabun",
				},
			};
			// console.log('docDefinition >> ' ,docDefinition)
			
			pdfMake.createPdf(docDefinition).open();
		},
		async initBody(invItems,owner){
			let bodies = _.chain(invItems)
				.filter((item, idx) => {
					
			
						let seller = owner == "โรงเรียน" ? 1 : 2;
						// console.log("check seller", seller, item.product, owner);
						return seller == item.product.seller;
					
				})
				.map((item, idx) => {
					let content = {
						index: idx + 1,
						name: `${item.product.name} ${item.option.name}`,
						price: item.option.price,
						quantity: item.quantity,
						total: item.quantity * item.option.price,
						note: item.note,
					};
					return [
						{ text: content.index, alignment: "center" },
						{ text: content.name, alignment: "center" },
						{ text: content.price, alignment: "center" },
						{ text: content.quantity, alignment: "center" },
						{ text: content.total, alignment: "center" },
						{ text: content.note, alignment: "center" },
					];
				})
				.value();
				return bodies;
		},
		async calTotalPrice(invItems,owner){
			let totalPrice = _.chain(invItems).filter((item, idx) => {
				if (!["โรงเรียน", "ร้านค้า"].includes(owner)) {
					return true;
				}
				let seller = owner == "โรงเรียน" ? 1 : 2;
				// console.log("check seller", seller, item.product, owner);
				return seller == item.product.seller;
			}).reduce((sum,item,idx)=>{
				sum+= item.quantity * item.option.price
				return sum;
			},0).value()
			return totalPrice;
		},
		async initConTent(owner,bodies,invoice,totalPrice){
			var tableObject = 	{
				style: "tableExample",
				color: "#444",
				alignment: "center",
				pageBreak:"after",
				table: {
					widths: ["auto", "*", "auto", "auto", "auto", "auto"],
					headerRows: 5,
					body: [
						[{ text: "เลขที่ใบสั่งซื้อ", bold: true, style: "tableHeader", alignment: "center" }, { text: invoice.id, colSpan: 2, alignment: "center" }, {}, { text: "วันที่ออกใบ", bold: true, style: "tableHeader", alignment: "center" }, { text: dayjs(invoice.issued_date).locale("th").format("DD MMM BBBB HH:mm:ss"), colSpan: 2, alignment: "center" }, {}],
						[{ text: "ชื่อ", bold: true, style: "tableHeader", alignment: "center" }, { text: invoice.user.firstname, colSpan: 2, alignment: "center" }, {}, { text: "นามสกุล", bold: true, style: "tableHeader", alignment: "center" }, { text: invoice.user.lastname, colSpan: 2, alignment: "center" }, {}],
						[
							{ text: "ชั้น", style: "tableHeader", alignment: "center" },
							{ text: `ม. ${invoice.user.grade}/${invoice.user.class}`, alignment: "center" },
							{ text: "เลขที่", style: "tableHeader", alignment: "center" },
							{ text: invoice.user.class_number, alignment: "center" },
							{ text: "เบอร์โทรศัพท์", style: "tableHeader", alignment: "center" },
							{ text: invoice.user.phone_number, alignment: "center" },
						],
						[{ text: "ใบสั่งจองสินค้า ปีการศึกษา 2565", style: "tableHeader", alignment: "center", colSpan: 6 }, {}, {}, {}, {}, {}],
						[
							{ text: "ลำดับ", style: "tableHeader", alignment: "center" },
							{ text: "รายการ", style: "tableHeader", alignment: "center" },
							{ text: "ราคา (บาท)", style: "tableHeader", alignment: "center" },
							{ text: "จำนวน", style: "tableHeader", alignment: "center" },
							{ text: "รวมเป็นเงิน (บาท)", style: "tableHeader", alignment: "center" },
							{ text: "หมายเหตุ", style: "tableHeader", alignment: "center" },
						],
						...bodies,
						[
							{ text: "รวมเป็นเงิน", style: "tableHeader", alignment: "center" },
							{ text: totalPrice, alignment: "center" },
							{ text: "ส่วนลด", style: "tableHeader", alignment: "center" },
							{ text: invoice.user.discount?.value ?? 0, alignment: "center" },
							{ text: "ที่ต้องชำระ", style: "tableHeader", alignment: "center" },
							{ text: this.lastPrice(totalPrice, invoice.user.discount?.value ?? 0), alignment: "center" },
						],
						[{ text: "ลงชื่อผู้รับเงิน", style: "tableHeader", alignment: "center", colSpan: 5 }, {}, {}, {}, {}, { text: "", alignment: "center" }],
					],
				},
			}


			const data = [
				{ text: "โรงเรียนราชินีบูรณะ จังหวัดนครปฐม", fontSize: 18, alignment: "center", style: "header", bold: true, width: "*" },
			"",
			{ text: "ใบสั่งซื้อสำหรับ" + owner, style: "subheader", fontSize: 12, alignment: "center" },
			"",tableObject,
			{ width: "*", text: "" },]
			return data;
		},

		downloadProductSummary(owner,products,grade = -1,classes = -1){
			this.initPDF()
			let subheader = "สรุปรายการสินค้า"
			if(grade != -1) subheader += "ประจำชั้นปีที่ " + grade + " "
			if(classes != -1) subheader += "ห้อง " + classes + " "
			subheader+= `สำหรับ${owner}`
			let body = _.map(products, item => {
				let content = {
					index: item.index,
					name: item.name,
					option: item.option,
					quantity: item.quantity,
					price: item.price,
					total: item.total,
				};
				return [
					{ text: content.index, alignment: "center" },
					{ text: content.name, alignment: "left" },
					{ text: content.option, alignment: "center" },
					{ text: content.quantity, alignment: "center" },
					{ text: content.price, alignment: "center" },
					{ text: content.total, alignment: "center" },
				];
			});
			
			let docDefinition = {
				pageSize: "A4",
				pageOrientation: "portrait",
				content: [
					{ text: "โรงเรียนราชินีบูรณะ จังหวัดนครปฐม", fontSize: 18, alignment: "center", style: "header", bold: true, width: "*" },
					"",
					{ text: subheader, style: "subheader", fontSize: 12, alignment: "center" },
					"",
					{
						style: "tableExample",
						color: "#444",
						alignment: "center",
						table: {
							widths: ["auto", "*", "auto", "auto", "auto", "auto"],
							headerRows: 1,
							body: [
								[
									{ text: "ลำดับ", style: "tableHeader", alignment: "center" },
									{ text: "รายการ", style: "tableHeader", alignment: "center" },
									{ text: "ตัวเลือก", style: "tableHeader", alignment: "center" },
									{ text: "จำนวน", style: "tableHeader", alignment: "center" },
									{ text: "ราคา/ชิ้น", style: "tableHeader", alignment: "center" },
									{ text: "รวม", style: "tableHeader", alignment: "center" },
								],
								...body,
							],
						},
					},
					{ width: "*", text: "" },
				],
				styles: {
					header: {
						fontSize: 18,
						bold: true,
						margin: [0, 0, 0, 10],
					},
					subheader: {
						fontSize: 16,
						bold: true,
						margin: [0, 10, 0, 5],
					},
					tableExample: {
						margin: [0, 5, 0, 15],
					},
					tableHeader: {
						bold: true,
						fontSize: 13,
						color: "black",
					},
				},
				defaultStyle: {
					font: "Sarabun",
				},
			};
			pdfMake.createPdf(docDefinition).open();
		},
		async initConTentTableTotal(bodyTable,totalQuantityAll,totalPriceAll,grade = -1,classes = -1,owner){
			let subheader = "สรุปรายการสินค้า"
			if(grade != -1) subheader += "ประจำชั้นปีที่ " + grade + " "
			if(classes != -1) subheader += "ห้อง " + classes + " "
			subheader+= `สำหรับ${owner}`
			var tableObject = 	{
				style: "tableExample",
				color: "#444",
				alignment: "center",
				pageBreak:"after",
				table: {
					widths: ["*", "*", "auto", "auto", "auto"],
					headerRows: 1,
					body: [
						[
							{ text: "สรุปรายการสินค้า", bold: true, style: "tableHeader", alignment: "center" ,colSpan: 5},{},{},{},{}
						],
						[
							{ text: "รายการสินค้า", bold: true, style: "tableHeader", alignment: "center" },
							{ text: "ตัวเลือก", bold: true, style: "tableHeader", alignment: "center" },
							{ text: "ราคาตัวละ", bold: true, style: "tableHeader", alignment: "center" },
							{ text: "จำนวน", bold: true, style: "tableHeader", alignment: "center" },
							{ text: "รวมเป็นเงิน", bold: true, style: "tableHeader", alignment: "center" },
						],
						...bodyTable
						,[
							{ text: 'รวมทั้งหมด', bold: true, style: "tableHeader", alignment:  "right" ,colSpan: 3},
							{},
							{},
							{ text: totalQuantityAll, bold: true, style: "tableHeader", alignment: "center" },
							{ text: totalPriceAll, bold: true, style: "tableHeader", alignment: "center" },
						]
					],
				},
			}

			const data = [
				{ text: "สรุปรายการสินค้า", fontSize: 18, alignment: "center", style: "header", bold: true, width: "*" },
				"",
				{ text: subheader, style: "subheader", fontSize: 12, alignment: "center" },
				"",
				tableObject,
			{ width: "*", text: "" },]
			return data;
		}

	},
};
