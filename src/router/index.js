import Vue from "vue";
import VueRouter from "vue-router";
import Login from "../views/Login.vue";
import changePassword from "../views/ChangePassword.vue";
import Register from "../views/Register.vue";
import EditProfile from "../views/EditProfile.vue";
import users from "./users";
import products from "./products";
import Profile from "../views/Profile.vue";
import Dashboard from "../views/dashboard/index.vue";
import dashboardRoute from "./dashboard";

import Product from "../views/Product.vue";
import Cart from "../views/Cart.vue";

import StudentInvoice from "../views/StudentInvoice.vue";
import Summary from "../views/Summary.vue";
import SummaryDashboard from "../views/SummaryDashboard.vue";
import SumSchool from "../views/SumSchool.vue";
import SumStore from "../views/SumStore.vue";

import UMA from "../views/MA.vue";

Vue.use(VueRouter);

const routes = [
	{
		path: "/login",
		name: "Login",
		component: Login,
		meta: {
			auth: false,
		},
	},
	{
		path: "/changepassword",
		name: "ChangePassword",
		component: changePassword,
		meta: {
			auth: true,
		},
	},
	{
		path: "/register",
		name: "Register",
		component: Register,
		meta: {
			auth: false,
		},
	},
	{
		path: "/edit/profile",
		name: "EditProfile",
		component: EditProfile,
		meta: {
			auth: true,
		},
	},
	{
		path: "/Profile",
		name: "Profile",
		component: Profile,
		meta: {
			auth: true,
		},
	},
	{
		path: "/",
		name: "products",
		component: Product,
		meta: {
			auth: true,
		},
	},
	{
		path: "/myinvoices",
		name: "invoices.student",
		component: StudentInvoice,
		meta: {
			auth: true,
		},
	},
	{
		path: "/cart",
		name: "cart",
		component: Cart,
		meta: {
			auth: true,
		},
	},
	{
		path: "/SummaryDashboard",
		name: "SummaryDashboard",
		component: SummaryDashboard,
		meta: {
			auth: ['teacher','admin'],
		},
	},
	{
		path: "/SumSchool",
		name: "SumSchool",
		component: SumSchool,
		meta: {
			auth: ['teacher','admin'],
		},
	},
	{
		path: "/SumStore",
		name: "SumStore",
		component: SumStore,
		meta: {
			auth: ['teacher','admin'],
		},
	},
	{
		path: "/uma",
		name: "UMA",
		component: UMA,
	
	},
	{
		path: "/dashboard",
		name: "dashboard",
		component: Dashboard,
		children: [...dashboardRoute],
		meta: {
			auth: true,
		},
	},
	...users,
	...products,
	{
		path: "/404",
		component: () => import("../views/404.vue"),
	},
	{
		path: "/403",
		component: () => import("../views/403.vue"),
	},

	{
		path: "*",
		redirect: '/404'
	},
];

const router = new VueRouter({
	mode: "history",
	// base: process.env.BASE_URL,
	routes,
});

export default router;
