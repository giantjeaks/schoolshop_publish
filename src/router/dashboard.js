import users from "./users";
import products from "./products";
import ProductsIndex from "../views/products/index.vue";
import UserIndex from "../views/users/index.vue";
import InvoiceIndex from "../views/invoices/index.vue";
import DiscountIndex from "../views/discounts/index.vue";
const router = [
	{
		path: "products",
		name: "products.index",
		component: ProductsIndex,
		meta: {
			auth: ['teacher','admin'],
		},
		// children:[...products]
	},
	{
		path: "users",
		name: "users.index",
		component: UserIndex,
		meta: {
			auth: ['admin'],
		},
		// children: [...users],
	},
	{
		path: "invoices",
		name: "invoices.index",
		component: InvoiceIndex,
		meta: {
			auth: ['teacher','admin'],
		},
	},
    {
		path: "discounts",
		name: "discounts.index",
		component: DiscountIndex,
		meta: {
			auth: ['teacher','admin'],
		},
	},
];

export default router;
