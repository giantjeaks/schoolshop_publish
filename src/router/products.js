import Index from "../views/products/index.vue"
import Edit from "../views/products/edit.vue"
import Create from "../views/products/create.vue"
const router = [
    {
        path : '/dashboard/products/edit/:id',
        name : 'products.edit',
        component : Edit,
        meta : {
            auth : ['admin','teacher']
        }
    },
    {
        path : '/dashboard/products/create',
        name : 'products.create',
        component : Create,
        meta : {
            auth : ['admin','teacher']
        }
    }
];

export default router;