import Index from "../views/users/index.vue"
import Edit from "../views/users/edit.vue"
import Create from "../views/users/create.vue"
const router = [
    // {
    //     path : '',
    //     name : 'users.index',
    //     component : Index,
    //     meta : {
    //         auth : true
    //     }
    // },
    {
        path : '/dashboard/users/edit/:id',
        name : 'users.edit',
        component : Edit,
        meta : {
            auth : ['admin']
        }
    },
    {
        path : '/dashboard/users/create',
        name : 'users.create',
        component : Create,
        meta : {
            auth : ['admin']
        }
    }
];

export default router;