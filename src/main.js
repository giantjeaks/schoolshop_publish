import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";

import VueAxios from "vue-axios";
import axios from "axios";
import Vuelidate from "vuelidate";

import auth from '@websanova/vue-auth/dist/v2/vue-auth.esm.js';
import driverAuthBearer from '@websanova/vue-auth/dist/drivers/auth/bearer.esm.js';
import driverHttpAxios from '@websanova/vue-auth/dist/drivers/http/axios.1.x.esm.js';
import driverRouterVueRouter from '@websanova/vue-auth/dist/drivers/router/vue-router.2.x.esm.js';

import VueGtag from "vue-gtag";
Vue.use(VueGtag, {
	config: { id: process.env.VUE_APP_GTAG_KEY}
  });

import _ from "lodash";
import dayjs from "dayjs";
import 'dayjs/locale/th' // import locale
import timezone from "dayjs/plugin/timezone"
import buddhistEra from "dayjs/plugin/buddhistEra"
dayjs.extend(timezone)
dayjs.extend(buddhistEra)
dayjs.tz.setDefault("Asia/Bangkok")
dayjs.locale('th')
Vue.use(VueAxios, axios);
Vue.use(Vuelidate);

import VueSweetalert2 from 'vue-sweetalert2';

// If you don't need the styles, do not connect
import 'sweetalert2/dist/sweetalert2.min.css';

let options = {
    confirmButtonColor: '#3498DB',
    cancelButtonColor: '#BDC3C7',
    confirmButtonText: "ยืนยัน",
    cancelButtonText: "ยกเลิก",
}

Vue.use(VueSweetalert2,options);

Vue.config.productionTip = false;

Vue.use(auth, {
	plugins: {
		http: axios, // Axios
		router: router,
	},
	drivers: {
		auth: driverAuthBearer,
		http: driverHttpAxios,
		router: driverRouterVueRouter,
	},
	options: {
		rolesKey: 'roles',
		authRedirect: { path: "/login" },
		// forbiddenRedirect: function (transition) {
		// 	return "/";
		// },
		// notFoundRedirect: function (transition) {
		// 	return "/404";
		// },
		loginData: { url: "/api/v1/auth/login", method: "POST", redirect: "/", fetchUser: true, staySignedIn: true },
		logoutData: { url: "/api/v1/auth/logout", method: "GET", redirect: "/login", makeRequest: true },
		fetchData: { url: "/api/v1/auth/profile", method: "GET", enabled: true },
		refreshData: { url: "/api/v1/auth/refresh", method: "GET", enabled: true, interval: 30 },
		parseUserData(res){
			return res
		}
	},
});

const handleResponseError = (error) => {
	if (error.response.status===429) {
		Vue.swal({
			title:'เกิดข้อผิดพลาด',
			icon:'error',
			text:'เนื่องจากผู้ใช้ส่งคำขอมากเกินไปในระยะเวลาที่กำหนด (อาจเป็นข้อผิดพลาดจากปลั๊กอิน,DDos ,หรืออย่างอื่น) ซึ่งทางเซิร์ฟเวอร์จะให้คุณหยุดส่งคำขอ'
		})
	}
    return Promise.reject(error)
}

axios.defaults.baseURL = process.env.VUE_APP_URL;
axios.defaults.headers.common["Accept"] = "application/json";
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
axios.interceptors.response.use((response) => response, handleResponseError)

window.axios = axios;
window._ = _;
window.dayjs = dayjs
new Vue({
	router,
	store,
	vuetify,
	render: (h) => h(App),
}).$mount("#app");
